package ie.ucd.party;

import ie.ucd.items.AlcoholicDrink;
import ie.ucd.items.Drink;
import ie.ucd.items.Food;
import ie.ucd.people.Person;

// represents a person who cannot drink alcohol
public class NotDrinker extends Person {
	
	// name of instance
	protected java.lang.String name;

	// constructor that requires a name for the person to be given
	public NotDrinker(java.lang.String name) {
		this.name = name;
	}

	// returns false if drink is alcoholic, returns false otherwise
	public boolean drink(Drink arg0) {
		if(arg0 instanceof AlcoholicDrink) {
			System.out.println(this.name + ": Hell no! I ain't drinking no " + arg0.getName());
			return false;
		}
		else {
			System.out.println(this.name + ": Mmmm! I love " + arg0.getName());
			return true;
		}
	}

	// always returns true
	public boolean eat(Food arg0) {
		return true;
	}

}
