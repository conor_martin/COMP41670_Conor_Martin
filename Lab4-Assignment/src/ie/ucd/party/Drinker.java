package ie.ucd.party;

import ie.ucd.items.AlcoholicDrink;
import ie.ucd.items.Drink;
import ie.ucd.items.Food;
import ie.ucd.people.Person;

// represents a person who can drink alcohol
public class Drinker extends Person {
	
	// counter for # alcoholic drinks consumed
	protected int numberOfDrinks;
	// name of instance
	protected java.lang.String name;

	// constructor that requires a name for the person to be given
	public Drinker(java.lang.String name) {
		this.name = name;
		numberOfDrinks = 0;
	}

	// increments numberOfDrinks counter if drink is alcoholic, always returns true
	public boolean drink(Drink arg0) {
		if(arg0 instanceof AlcoholicDrink) {
			numberOfDrinks++;
			System.out.println(this.name + ": Oh wow, I really can't get enough " + arg0.getName());
		}
		else {
			System.out.println(this.name + ": This is okay, but I really prefer alcohol to " + arg0.getName());
		}
		return true;
	}

	// always returns true
	public boolean eat(Food arg0) {
		return true;
	}
	
	// returns true if instance has drank more alcoholic drinks than weight/10, returns false otherwise
	public boolean isDrunk() {
		if(this.numberOfDrinks > (this.getWeight() / 10)) {
			System.out.println(this.name + ": It's true, I'm pretty drunk!");
			return true;
		}
		else {
			System.out.println(this.name + ": I could keep drinking, I'm not drunk yet!");
			return false;
		}
	}

}
