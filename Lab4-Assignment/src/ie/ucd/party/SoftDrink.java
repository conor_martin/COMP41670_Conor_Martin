package ie.ucd.party;

import ie.ucd.items.NotAlcoholicDrink;

//represents a SoftDrink, which is NotAlcoholic
public class SoftDrink extends NotAlcoholicDrink {

	// constructor which requires a name and volume for the SoftDrink
	public SoftDrink(String name, double volume) {
		super(name, volume);
		// TODO Auto-generated constructor stub
	}

}
