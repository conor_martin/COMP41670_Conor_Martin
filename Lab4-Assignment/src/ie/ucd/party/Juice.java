package ie.ucd.party;

import ie.ucd.items.NotAlcoholicDrink;

// represents a Juice Drink, which is NotAlcoholic
public class Juice extends NotAlcoholicDrink {

	// constructor which requires a name and volume for the juice
	public Juice(String name, double volume) {
		super(name, volume);
		// TODO Auto-generated constructor stub
	}

}
