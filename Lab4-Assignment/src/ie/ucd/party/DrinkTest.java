package ie.ucd.party;

import java.util.ArrayList;
import java.util.List;

import ie.ucd.items.Drink;
import ie.ucd.items.Wine;
import ie.ucd.items.WineType;
import ie.ucd.people.Person;

// class with main function to test Drinker and NotDrinker classes
public class DrinkTest {

	public static void main(String[] args) {
		// instantiate a Drinker and NonDrinker
		List<Person> myPeople = new ArrayList<>();
		Person ruth = new NotDrinker("Ruth");
		Person conor = new Drinker("Conor");
		
		// add People to a list
		myPeople.add(conor);
		myPeople.add(ruth);
		
		// set weights for People
		conor.setWeight(30.0);
		ruth.setWeight(20.0);
		
		// instantiate some Alcoholic and NotAlcoholic Drinks
		List<Drink> myDrinks = new ArrayList<>();
		Drink redWine = new Wine("Pinot Noir", 250.0, 12.5, WineType.Red);
		Drink whiteWine = new Wine("Chardonnay", 350.0, 14.0, WineType.White);
		Drink roseWine = new Wine("Jacob's Creek", 300.0, 13.4, WineType.Rose);
		Drink orangeJuice = new Juice("Tropicana", 500.0);
		Drink pepsi = new SoftDrink("Pepsi MAX", 1000.0);
		
		// add Drinks to a list
		myDrinks.add(redWine);
		myDrinks.add(whiteWine);
		myDrinks.add(roseWine);
		myDrinks.add(orangeJuice);
		myDrinks.add(pepsi);
		
		// iterate through list of people
		for (Person p : myPeople) {
			// iterate through list of drinks and attempt to make current person drink
			for (Drink d : myDrinks) {
				p.drink(d);
			}
			// if current person is a Drinker, then check if they are drunk
			if (p instanceof Drinker) {
				((Drinker) p).isDrunk();
				// give them another drink to get them drunk and check again!
				p.drink(roseWine);
				((Drinker) p).isDrunk();
			}
		}
		
	}

}
