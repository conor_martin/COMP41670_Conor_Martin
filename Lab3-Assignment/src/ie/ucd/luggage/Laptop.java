package ie.ucd.luggage;

/**
 * Class representing a safe Object that implements the Item interface
 * @author Conor
 *
 */
public class Laptop implements Item {
	
	protected String type;
	protected double weight;

	/**
	 * 
	 * @param newType Type of Laptop
	 * @param newWeight Weight of Laptop
	 */
	public Laptop(String newType, double newWeight) {
		this.type = newType;
		this.weight = newWeight;
	}

	public String getType() {
		return type;
	}

	public double getWeight() {
		return weight;
	}

	public boolean isDangerous() {
		return false;
	}

}
