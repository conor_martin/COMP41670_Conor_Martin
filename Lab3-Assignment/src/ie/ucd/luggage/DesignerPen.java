package ie.ucd.luggage;

/**
 * Subclass of Pen
 * @author Conor
 *
 */
public class DesignerPen extends Pen {
	
	protected String brand;

	/**
	 * 
	 * @param newBrand Brand of DesignerPen
	 * @param newWeight Weight of DesignerPen
	 */
	public DesignerPen(String newBrand, double newWeight) {
		super(newBrand, newWeight);
	}
	
	public String getType() {
		return brand;
	}

}
