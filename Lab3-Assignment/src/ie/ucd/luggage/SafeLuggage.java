package ie.ucd.luggage;

/**
 * Class that only allows users to add or remove items from bag if they have the correct password
 * @author Conor
 *
 */
public class SafeLuggage extends Suitcase {
	
	/**
	 * Create a password String to control access to superclass add and removeItem methods
	 */
	private String password;

	/**
	 * 
	 * @param newWeight The weight of the empty bag
	 * @param newMax The maximum weight allowed in the bag
	 * @param newPass The password to access superclass add and removeItem
	 */
	public SafeLuggage(double newWeight, double newMax, String newPass) {
		super(newWeight, newMax);
		this.password = newPass;
	}
	
	/**
	 * 
	 * @param item The Item to be added to the bag
	 * @param givenPassword The password used to attempt to gain access
	 */
	public void add(Item item, String givenPassword){
		if(givenPassword == this.password) {
			super.add(item);
		}
		else System.out.println("Error: Incorrect password, not adding " + item + " to luggage.\n");
	}
	
	/**
	 * 
	 * @param index The index to remove an Item from
	 * @param givenPassword The password used to attempt to gain access
	 */
	public void removeItem(int index, String givenPassword) {
		if(givenPassword == this.password) {
			super.removeItem(index);
		}
		else System.out.println("Error: Incorrect password, not removing item at index " + index + " from luggage.\n");
	}
}
