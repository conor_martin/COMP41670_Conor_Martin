package ie.ucd.luggage;

/**
 * Class representing a dangerous Object that implements the Item interface
 * @author Conor
 *
 */
public class Bomb implements Item {
	
	protected String type;
	protected double weight;	

	/**
	 * 
	 * @param newType Type of Bomb
	 * @param newWeight Weight of Bomb
	 */
	public Bomb(String newType, double newWeight) {
		this.type = newType;
		this.weight = newWeight;
	}

	public String getType() {
		return type;
	}

	public double getWeight() {
		return weight;
	}

	public boolean isDangerous() {
		return true;
	}

}
