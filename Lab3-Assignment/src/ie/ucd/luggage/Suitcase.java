package ie.ucd.luggage;

/**
 * A subclass of Luggage that can be instantiated 
 * @author Conor
 *
 */
public class Suitcase extends Luggage {
	
	private double bagWeight;
	private double maxWeight;

	/**
	 * 
	 * @param newWeight The weight of the empty bag
	 * @param newMax The max allowed weight in the bag
	 */
	public Suitcase(double newWeight, double newMax) {
		this.bagWeight = newWeight;
		this.maxWeight = newMax;
	}

	public double getBagWeight() {
		return bagWeight;
	}

	public double getMaxWeight() {
		return maxWeight;
	}

}
