package ie.ucd.luggage;

/**
 * Class to verify other classes
 * @author Conor
 *
 */
public class PackLuggage {

	public static void main(String[] args) {
		/**
		 * Make a correct and incorrect password
		 */
		String password = "Conor1234";
		String wrongPassword = "1234Conor";
		
		/**
		 * Create a SafeLuggage instance and instances of several Items
		 */
		
		SafeLuggage myLuggage = new SafeLuggage(10, 100, password);
		
		Item myLaptop = new Laptop("Toshiba", 10);
		Item myPen = new Pen("BIC", 1);
		Item myDesignerPen = new DesignerPen("Parker", 2);
		Item myBomb = new Bomb("TNT", 20);
		Item myLaptop2 = new Laptop("Dell", 20);
		Item myPen2 = new Pen("Biro", 5);
		Item myDesignerPen2 = new DesignerPen("Fancy", 7);
		Item myBomb2 = new Bomb("Semtex", 50);
		
		/**
		 * Attempt to add the Items to the SafeLuggage instance
		 */
		
		myLuggage.add(myLaptop, password);
		myLuggage.add(myDesignerPen, password);
		myLuggage.add(myPen, password);
		myLuggage.add(myBomb, password);
		myLuggage.add(myLaptop2, wrongPassword);
		myLuggage.add(myDesignerPen2, wrongPassword);
		myLuggage.add(myPen2, wrongPassword);
		myLuggage.add(myBomb2, wrongPassword);
		
		/**
		 * Print details of SafeLuggage instance contents
		 */
		
		System.out.println("BagWeight = " + myLuggage.getBagWeight() + "\n");
		System.out.println("MaxBagWeight = " + myLuggage.getMaxWeight() + "\n");
		System.out.println("Weight = " + myLuggage.getWeight() + "\n");
		System.out.println("Contents = " + myLuggage.getContents() + "\n");
		System.out.println("Bag is dangerous = " + myLuggage.isDangerous() + "\n");
		
		/**
		 * Attempt to remove Items from SafeLuggage instance
		 */
		
		myLuggage.removeItem(3, password);
		myLuggage.removeItem(0, wrongPassword);
		
		/**
		 * Check if SafeLuggage instance has any remaining dangerous Items
		 */
		
		System.out.println("Bag is dangerous = " + myLuggage.isDangerous() + "\n");
		
		/**
		 * Attempt to exceed max weight allowed in bag
		 */
		myLuggage.add(myBomb2, password);
		System.out.println("Weight = " + myLuggage.getWeight() + "\n");
		myLuggage.add(myBomb2, password);
	}

}
